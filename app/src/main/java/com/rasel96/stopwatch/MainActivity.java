package com.rasel96.stopwatch;

import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Chronometer chronometer;
    private Button play;
    public boolean isPlaying = false, isPaused = false;
    public long elapsedTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        chronometer = findViewById(R.id.chronometer);
        play = findViewById(R.id.play);
        play.setOnClickListener(this);
        elapsedTime = 0;
    }

    @Override
    public void onClick(View v) {
        if (!isPlaying && !isPaused){
            isPlaying = true;
            play.setText(R.string.pause);
            Toast.makeText(MainActivity.this, "Started!", Toast.LENGTH_LONG).show();

            chronometer.setBase(SystemClock.elapsedRealtime());
            chronometer.start();

            return;
        }
        if (isPlaying){
            isPaused = true;
            isPlaying = false;
            play.setText(R.string.resume);
            Toast.makeText(MainActivity.this, "Paused!", Toast.LENGTH_LONG).show();

            chronometer.stop();
            elapsedTime = SystemClock.elapsedRealtime() - chronometer.getBase();

            return;
        }
        if (isPaused){
            isPaused = false;
            isPlaying = true;
            play.setText(R.string.pause);
            Toast.makeText(MainActivity.this, "Resumed!", Toast.LENGTH_LONG).show();

            chronometer.setBase(SystemClock.elapsedRealtime() - elapsedTime);
            chronometer.start();

            return;
        }
    }

    public void resetMeter(View v){
        isPlaying = false;
        isPaused = false;
        play.setText(R.string.start);
        Toast.makeText(MainActivity.this, "Timer has been reset.", Toast.LENGTH_LONG).show();

        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.stop();
    }

}
